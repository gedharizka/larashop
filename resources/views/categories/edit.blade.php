@extends('layouts.global')

@section('title') Edit Category @endsection

@section ('content')
<h3>Halaman Edit</h3>


<div class="col-md-8">
  @if(session('status'))
    <div class="alert alert-success">
      {{session('status')}}
    </div>
  @endif 

  <form 
    action="{{route('categories.update',[$category->id])}}"
    enctype="multipart/form-data"
    method="POST"
    class="bg-white shadow-sm p-3">
    @csrf

    <input
      type="hidden"
      value="PUT"
      name="_method">

    <label for="">Category Name</label><br>
    <input
      class="form-control" 
      type="text"
      name="name"
      value="{{$category->name}}"><br><br>

    <label for="">Category Slug</label><br>
    <input
      class="form-control" 
      type="text"
      name="slug"
      value="{{$category->slug}}"><br><br>

    <label for="">Category Image</label><br>
    @if($category->image)
      <span>Current image</span><br>
      <img src="{{asset('storage/'. $category->image)}}" width="120px"><br><br>
    @endif
    <input
      class="form-control" 
      type="file"
      name="image">
      <small class="text-muted">Kosongkan jika tidak ingin mengubah gambar</small>
      <br><br>
    <input type="submit" class="btn btn-primary" value="Update">

    </form>
</div>
@endsection