@extends("layouts.global")

@section("title")create User @endsection

@section("content")

  <div class="col-md-8">

  @if(session('status'))
    <div class="alert alert-success">
      {{session('status')}}
    </div>
  @endif
  
    <form
      action="{{route('users.store')}}"
      enctype="multipart/form-data"
      class="bg-white shadow-sm p-3"
      method="POST"
    >
    @csrf
    <label for="name">Name</label>
    <input 
      class="form-control"
      placeholder="Full Name"
      type="text"
      id="name"
      name="name">
      <br>

    <label for="name">User Name</label>
    <input 
      class="form-control"
      placeholder="username"
      type="text"
      id="username"
      name="username">
      <br>

    <label for="name">Roles</label>
    <input 
      class="form-control"
      type="checkbox"
      name="roles[]"
      id="ADMIN"
      value="ADMIN">
      <label for="ADMIN">Administrator</label>

    <input 
      class="form-control"
      type="checkbox"
      name="roles[]"
      id="STAFF"
      value="STAFF">
    <label for="STAFF">Staff</label>

    <input 
      class="form-control"
      type="checkbox"
      name="roles[]"
      id="CUSTOMER"
      value="CUSTOMER">
    <label for="CUSTOMER">Customer</label>
      <br>

      <br>
    <label for="phone">Phone Nummber</label>
      <br>

      <input 
        type="text"
        name="phone"
        class="form-control">

        <br>
        <label for="address">Address</label>
        <textarea name="address" id="address" class="form-control"></textarea>

        <br>
        <label for="avatar">Avatar image</label>
        <br>
        <input 
          type="file"
          id="avatar"
          name="avatar"
          class="form-control">

        <hr class="my-3">

        <label for="email">Email</label>
        <input 
          type="text"
          class="form-control"
          placeholder="user@mail.com"
          name="email"
          id="email">
          <br>

        <label for="email">Password</label>
        <input 
          type="password"
          class="form-control"
          placeholder="password"
          name="password"
          id="password">
          <br>

        <label for="password_confirmation">Password Confirmation</label>
        <input 
          type="password"
          class="form-control"
          placeholder="password confirmation"
          name="password_confirmation"
          id="password_confirmation">
          <br>

        <input type="submit" value="save" class="btn btn-primary">

    </form>
  </div>

@endsection