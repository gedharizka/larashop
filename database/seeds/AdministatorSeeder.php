<?php

use Illuminate\Database\Seeder;

class AdministatorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $administrator = new \App\User; //membuat intance baru
        $administrator->username="administrator";
        $administrator->name ="Site Administrator";
        $administrator->email ="administrator@larashop.test";
        $administrator->roles = json_encode(["ADMIN"]);
        $administrator->password= \Hash::make("larashop");
        $administrator->phone="081234567890123";
        $administrator->avatar = "saat-ini-tidak-ada-file.png";
        $administrator->address ="Sarmili, Bintar, Tangsel";

        $administrator->save();

        $this->command->info("User admin berhasil ditambahkan");
    }
}
